.. _tests:

Tests
=====

We use `pytest <https://docs.pytest.org/en/latest/>`_ as a testing framework
and `Tox <https://tox.readthedocs.io>`_ as a test environment manager.
Currently, tests reside in the  `testing/` folder and some of them need a
couchdb server to be run against.

If you do have a couchdb server running on localhost on default port, the
following command should be enough to run tests::

    tox

CouchDB dependency
------------------

It is important to note that certain tests in the suite depend on specific
privileges on behalf of couchdb, such as for the creation of databases. 
In order to best approach this, it is important to configure couchdb to be
in `admin party mode <http://guide.couchdb.org/draft/security.html>`_. This will
grant all users all privileges, allowing the tests to execute without running 
into permission based exceptions.     

.. note::  You might want to reconfigure your couchdb instance to stop using admin party mode once testing is done.

In case you want to use a couchdb on another host or port, use the
`--couch-url` parameter for `pytest`::

    tox -- --couch-url=http://couch_host:5984

If you want to exclude all tests that depend on couchdb, deselect tests marked
with `needs_couch`::

    tox -- -m 'not needs_couch'

Benchmark tests
---------------

A set of benchmark tests is provided to measure the time and resources taken to
perform some actions. See the :ref:`documentation for benchmarks <benchmarks>`.
